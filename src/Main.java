import com.sun.source.doctree.SystemPropertyTree;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class PrintedProduct {
    //Описание класса PrintedProduct, предостовляющего печатный продукт

    // Поля класса
    private String title;
    private  String author;
    private String isbn;
    private int quantity;
    private double price;

    // Конструктор класса для создания экземпляра PrintedProduct
    public PrintedProduct(String title, String author, String isbn, int quantity, double price) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.quantity = quantity;
        this.price = price;
    }

    // Переопределение метода toString() для удобного вывода информации о продукте
    @Override
    public String toString() {
        return "PrintedProduct{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", isbn='" + isbn + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }

    // Геттеры для получения значений полей класса
    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }
}

public class PrintedWareHouse {
    //Основной класс PrintedWarehouse, представляющий склад печатной продукции

    //Хранилище продуктов, представленное в виде карты(ISBN- продукт)
    private Map<String, PrintedProduct> inventory;
    private Scanner scanner;

    // Конструктор класса
    public PrintedWareHouse() {
        inventory = new HashMap<>();
        scanner = new Scanner(System.in);
    }

    // Метод для добавления продукта в инвентярь
    public void addProduct() {
        System.out.println("Введите название:");
        String title = scanner.nextLine();
        System.out.println("Введите Автора:");
        String author = scanner.nextLine();
    }
}